<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\modules\admin\Settings;
use app\modules\admin\Articles;
use app\modules\admin\Pages;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
     public function init()
     {
         parent::init();
         $settings = Settings::find()->one();
         if ($settings) {
           $session = Yii::$app->session;
           if (!$session->has('setLang')) {
             \app\components\User::onLanguageChanged($settings->default_language);
             $session->set('setLang', $settings->default_language);
           }
         }

         #add your logic: read the cookie and then set the language
     }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
      $articles = Articles::find()->all();
      $pages = Pages::find()->all();

        return $this->render('index', [
            'articles' => $articles,
            'pages' => $pages,
        ]);
    }

    public function actionArticle($id)
    {
      $article = Articles::findOne($id);
      if ($article) {
          $keywords = "keywords_".Yii::$app->language;
          $description = "description_".Yii::$app->language;
          if (!empty($article->$keywords)) {
            $keyword = $article->$keywords;
          } else {
            $keyword = $article->keywords;
          }
          if (!empty($article->$description)) {
            $desc = $article->$description;
          } else {
            $desc = $article->keywords;
          }
          \Yii::$app->view->registerMetaTag(['name' => 'description','content' => $desc]);
          \Yii::$app->view->registerMetaTag(['name' => 'keywords','content' => $keyword]);
          return $this->render('article', [
              'article' => $article
          ]);
      } else {
        throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
      }

    }
    public function actionPage($id)
    {
      $page = Pages::findOne($id);
      if ($page) {
          $keywords = "keywords_".Yii::$app->language;
          $description = "description_".Yii::$app->language;
          if (!empty($page->$keywords)) {
            $keyword = $page->$keywords;
          } else {
            $keyword = $page->keywords;
          }
          if (!empty($page->$description)) {
            $desc = $page->$description;
          } else {
            $desc = $page->keywords;
          }
          \Yii::$app->view->registerMetaTag(['name' => 'description','content' => $desc]);
          \Yii::$app->view->registerMetaTag(['name' => 'keywords','content' => $keyword]);
          return $this->render('page', [
              'page' => $page
          ]);
      } else {
        throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
      }

    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
