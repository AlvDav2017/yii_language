<?php

namespace app\modules\admin;

/**
 * admin module definition class
 */
class User extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
      if (!\Yii::$app->user->isGuest) {
        parent::init();
      } else {
        return \Yii::$app->response->redirect(['site/login']);
      }
    }
}
