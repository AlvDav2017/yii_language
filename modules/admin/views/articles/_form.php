<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\bootstrap\Tabs;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Articles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articles-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    $tab_en = $form->field($model, 'title_en')->textInput(['maxlength' => true])
              .$form->field($model, 'keywords_en')->textInput(['maxlength' => true])
              .$form->field($model, 'description_en')->textarea(['rows' => 6])
              .$form->field($model, 'content_en')->widget(CKEditor::className(),[
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]);

    $tab_ru = $form->field($model, 'title_ru')->textInput(['maxlength' => true])
              .$form->field($model, 'keywords_ru')->textInput(['maxlength' => true])
              .$form->field($model, 'description_ru')->textarea(['rows' => 6])
              .$form->field($model, 'content_ru')->widget(CKEditor::className(),[
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]);

    $tab_de = $form->field($model, 'title_de')->textInput(['maxlength' => true])
              .$form->field($model, 'keywords_de')->textInput(['maxlength' => true])
              .$form->field($model, 'description_de')->textarea(['rows' => 6])
              .$form->field($model, 'content_de')->widget(CKEditor::className(),[
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]);

     ?>
    <div class="col-sm-8">
      <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
      <?= $form->field($model, 'content')->widget(CKEditor::className(),[
                'editorOptions' => [
                    'preset' => 'full',
                    'inline' => false,
                ],
            ]); ?>
      <div class="form-group">
          <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <?php

        Modal::begin([
            'size'=>'modal-lg',
            'header' => '',
            'toggleButton' => ['label' => Yii::t('app', 'Open modal'),'class' => 'btn btn-warning'],
        ]);
        echo Tabs::widget([
            'items' => [
                [
                    'label' => 'En',
                    'content' => $tab_en,
                    'active' => true
                ],
                [
                    'label' => 'Ru',
                    'content' => $tab_ru,
                ],
                [
                    'label' => 'De',
                    'content' => $tab_de,
                ],
            ],
        ]);


        Modal::end();
         ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
