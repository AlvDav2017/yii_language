<?php

namespace app\modules\admin;

use Yii;

/**
 * This is the model class for table "Settings".
 *
 * @property int $id
 * @property string|null $title_ru
 * @property string|null $title_en
 * @property string|null $title_de
 * @property string|null $keywords_ru
 * @property string|null $keywords_en
 * @property string|null $keywords_de
 * @property string|null $description_ru
 * @property string|null $description_en
 * @property string|null $description_de
 * @property string|null $content_ru
 * @property string|null $content_en
 * @property string|null $content_de
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['default_language', 'active_languages'], 'string'],
            [['default_language', 'active_languages'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'default_language' => Yii::t('app', 'Default Language'),
            'active_languages' => Yii::t('app', 'Active Languages'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return SettingsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SettingsQuery(get_called_class());
    }
}
