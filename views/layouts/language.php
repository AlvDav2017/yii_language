<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\modules\admin\Settings;

$settings = Settings::find()->one();
$query="";
if (!empty($_SERVER['QUERY_STRING'])) {
  $query='?'.$_SERVER['QUERY_STRING'];
}

if (Yii::$app->user->isGuest && $settings) {
  if(strpos($settings->active_languages, "en") !== false){
    echo '<li class="lang">'.Html::a('En',Yii::$app->request->getBaseUrl(true).'/index.php/en/'.Yii::$app->controller->route.$query).'</li>';
  }
  if(strpos($settings->active_languages, "ru") !== false){
    echo '<li class="lang">'.Html::a('Ru',Yii::$app->request->getBaseUrl(true).'/index.php/ru/'.Yii::$app->controller->route.$query).'</li>';
  }
  if(strpos($settings->active_languages, "de") !== false){
    echo '<li class="lang">'.Html::a('De',Yii::$app->request->getBaseUrl(true).'/index.php/de/'.Yii::$app->controller->route.$query).'</li>';
  }
} else {
  echo '<li class="lang">'.Html::a('En',Yii::$app->request->getBaseUrl(true).'/index.php/en/'.Yii::$app->controller->route.$query).'</li>';
  echo '<li class="lang">'.Html::a('Ru',Yii::$app->request->getBaseUrl(true).'/index.php/ru/'.Yii::$app->controller->route.$query).'</li>';
  echo '<li class="lang">'.Html::a('De',Yii::$app->request->getBaseUrl(true).'/index.php/de/'.Yii::$app->controller->route.$query).'</li>';
}
?>
