<?php

use yii\bootstrap\Html;

$this->title = 'My Yii Application';
$title = "title_".Yii::$app->language;
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2 class="text-center"><?=Yii::t('app', 'Articles')?></h2>
                <?php foreach ($articles as $article): ?>
                  <?php if (!empty($article->$title)): ?>
                      <?=Html::a($article->$title,['/site/article','id'=>$article->id])?>
                  <?php else: ?>
                      <?=Html::a($article->title,['/site/article','id'=>$article->id])?>
                  <?php endif; ?>
                  <hr>
                <?php endforeach; ?>
            </div>
            <div class="col-lg-6">
              <h2 class="text-center"><?=Yii::t('app', 'Pages')?></h2>
                <?php foreach ($pages as $page): ?>
                  <?php if (!empty($page->$title)): ?>
                      <?=Html::a($page->$title,['/site/page','id'=>$page->id])?>
                  <?php else: ?>
                      <?=Html::a($page->title,['/site/page','id'=>$page->id])?>
                  <?php endif; ?>
                  <hr>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
</div>
