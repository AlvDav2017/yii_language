<?php
use yii\bootstrap\Html;

$title = "title_".Yii::$app->language;
$content_key = "content_".Yii::$app->language;

if (!empty($page->$title)) {
  $title_page = $page->$title;
} else {
  $title_page = $page->title;
}
if (!empty($page->$content_key)) {
  $content = $page->$content_key;
} else {
  $content = $page->content;
}

$this->title = $title_page;
?>
<div class="site-index">
    <div class="body-content">
      <h1 class="text-center"><?=$title_page?></h1>
      <hr>
      <?=$content?>
    </div>
</div>
