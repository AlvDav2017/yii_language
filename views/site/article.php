<?php
use yii\bootstrap\Html;

$title = "title_".Yii::$app->language;
$content_key = "content_".Yii::$app->language;

if (!empty($article->$title)) {
  $title_article = $article->$title;
} else {
  $title_article = $article->title;
}
if (!empty($article->$content_key)) {
  $content = $article->$content_key;
} else {
  $content = $article->content;
}

$this->title = $title_article;
?>
<div class="site-index">
    <div class="body-content">
      <h1 class="text-center"><?=$title_article?></h1>
      <hr>
      <?=$content?>
    </div>
</div>
