<?php
return [
  'About'=>"О нас",
  'Home'=>"Главная",
  'Contact'=>"Контакт",
  'Login'=>"Вход",
  'Logout'=>"Выход",
  'Articles'=>"Статьи",
  'Pages'=>"Страницы",
  'Settings'=>'Настройки',
  'Create Articles'=>'Создать Статьи',
  'Create Pages'=>'Создать Страницы',
  'Create Settings'=>'Создать Настройки',
  'Save'=>'Сохранить',
  'Update'=>'Редактировать',
  'Delete'=>'Удалить',
  'Open modal'=>'Открыть modal',


];
 ?>
