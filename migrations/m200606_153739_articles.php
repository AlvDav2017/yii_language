<?php
use yii\db\Migration;

/**
 * Class m200606_153739_articles
 */
class m200606_153739_articles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articles', [
          'id' => $this->primaryKey(),
          'title' => $this->string()->Null(),
          'title_ru' => $this->string()->Null(),
          'title_en' => $this->string()->Null(),
          'title_de' => $this->string()->Null(),
          'keywords' => $this->string()->Null(),
          'keywords_ru' => $this->string()->Null(),
          'keywords_en' => $this->string()->Null(),
          'keywords_de' => $this->string()->Null(),
          'description' => $this->text()->Null(),
          'description_ru' => $this->text()->Null(),
          'description_en' => $this->text()->Null(),
          'description_de' => $this->text()->Null(),
          'content' => $this->text()->Null(),
          'content_ru' => $this->text()->Null(),
          'content_en' => $this->text()->Null(),
          'content_de' => $this->text()->Null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200606_153739_articles cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }
*/
    public function down()
    {
      $this->dropTable('articles');
    }

}
