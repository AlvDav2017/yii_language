<?php

use yii\db\Migration;
use \app\modules\admin\Settings;
/**
 * Class m200608_100549_settings
 */
class m200608_100549_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createTable('settings', [
        'id' => $this->primaryKey(),
        'default_language' => $this->string()->Null(),
        'active_languages' => $this->string()->Null(),
      ]);

      $setting = new Settings;
      $setting->default_language="en";
      $setting->active_languages="en,ru,de";
      $setting->save();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200608_100549_settings cannot be reverted.\n";

        return false;
    }

    public function down()
    {
      $this->dropTable('settings');
    }

}
