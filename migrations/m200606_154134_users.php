<?php

use yii\db\Migration;
use app\models\User;
/**
 * Class m200606_154134_users
 */
class m200606_154134_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createTable('users', [
        'id' => $this->primaryKey(),
        'username' => $this->string()->Null(),
        'email' => $this->string()->unique(),
        'password_hash' => $this->string()->Null(),
        'password_reset_token' => $this->string()->unique(),
        'verification_token' => $this->string()->unique(),
        'auth_key' => $this->string()->unique(),
        'type' => $this->integer()->defaultValue(1),
      ]);

      $admin = new User;
      $admin->username="admin";
      $admin->email="admin@gmail.com";
      $admin->password="123456";
      $admin->password_hash=Yii::$app->security->generatePasswordHash(123456);
      $admin->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200606_154134_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }
    */
    public function down()
    {
      $this->dropTable('users');
    }

}
