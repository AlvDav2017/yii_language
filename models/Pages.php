<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string|null $title_ru
 * @property string|null $title_en
 * @property string|null $title_de
 * @property string|null $keywords_ru
 * @property string|null $keywords_en
 * @property string|null $keywords_de
 * @property string|null $description_ru
 * @property string|null $description_en
 * @property string|null $description_de
 * @property string|null $content_ru
 * @property string|null $content_en
 * @property string|null $content_de
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_ru', 'description_en', 'description_de', 'content_ru', 'content_en', 'content_de'], 'string'],
            [['title_ru', 'title_en', 'title_de', 'keywords_ru', 'keywords_en', 'keywords_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_en' => Yii::t('app', 'Title En'),
            'title_de' => Yii::t('app', 'Title De'),
            'keywords_ru' => Yii::t('app', 'Keywords Ru'),
            'keywords_en' => Yii::t('app', 'Keywords En'),
            'keywords_de' => Yii::t('app', 'Keywords De'),
            'description_ru' => Yii::t('app', 'Description Ru'),
            'description_en' => Yii::t('app', 'Description En'),
            'description_de' => Yii::t('app', 'Description De'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_en' => Yii::t('app', 'Content En'),
            'content_de' => Yii::t('app', 'Content De'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return PagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PagesQuery(get_called_class());
    }
}
